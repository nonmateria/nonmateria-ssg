
Music related links:

* [Xenharmonic Wiki](https://en.xen.wiki/w/Main_Page)

-----------------------

Want to code your own DSPs? First of all you should know that [real-time waits for nothing](http://www.rossbencina.com/code/real-time-audio-programming-101-time-waits-for-nothing)!

The second things you should do is to get a copy of Will Pirkle's books on designing audio fx and synthesizer plugins, all the basics are included.

Then you should check out this resources:

* The [musicdsp.org](https://www.musicdsp.org/en/latest/) archive
* Valhalla DSP resources on [coding reverbs](https://valhalladsp.com/category/learn/plugin-design-learn/)
* Some blog posts by [Urs Heckmann](https://urs.silvrback.com/archive)
* Paul Batchelor [sndkit](https://pbat.ch/sndkit/algos/) has some good c implementations
* [this](https://mu.krj.st/) interesting WIP tutorial
