
The internet is slowly becoming unsearchable, so take some time to curate a selection of links you care about =) 

-------------------------------------

Stashes of bookmarked pages:

* on [game developing](gamedev_links.html)
* on [pixel art](pixelart_links.html)
* on [music & DSPs](music_links.html)
* critical articles on [technology](tech_links.html)

-------------------------------------
Wikis, Digital Gardens, Portfolios, Blogs

* [analog nowhere](https://www.analognowhere.com/)
* [anarchestra](https://www.anarchestra.org/instruments)
* [androidarts.com](https://androidarts.com)
* [aeriform.io](https://www.aeriform.io/)
* [badd10de.dev](https://badd10de.dev)
* [cancel.fm](https://cancel.fm/)
* [deianeira.co](https://deianeira.co/)
* [devine lu linvega](http://wiki.xxiivv.com/)
* [everest pipkin](https://everest-pipkin.com/)
* [ghostlevel.net](https://ghostlevel.net/)
* [helvetica blanc](https://helveticablanc.com/)
* [inigo quilez](http://iquilezles.org/)
* [kokorobot.ca](https://kokorobot.ca)
* [nchrs.xyz](https://nchrs.xyz)
* [opacity.ru](https://opacity.ru/)
* [palomakop.tv](https://palomakop.tv/)
* [pbat.ch/](https://pbat.ch/)
* [rael zero](https://oddworlds.org)
* [recyclism.com](http://www.recyclism.com/)
* [sajan rai](http://www.sajanrai.co.uk/)
* [thomasorus](https://thomasorus.com)
* [text-mode](https://text-mode.org/)
* [toplap wiki](https://toplap.org/wiki/Main_Page)
* [viznut](http://viznut.fi/en/)

-------------------------------------
[ITA]

* [gekigemu](https://www.youtube.com/channel/UChQcap_4Iam2oR2DHzYj_Jg) (on youtube)
* [i fumetti della gleba](https://fumettidellagleba.org/)
* [light item](https://lightitem.bandcamp.com/)
* [LRNZ](https://www.lrnz.it/)
* [queer mushroom forest](https://queermushroomforest.weebly.com/)
* [radio merda malata](http://radiomerdamalata.scienceontheweb.net/) 
* [stelvio.xyz](https://www.stelvio.xyz/)
* [tele kenobit](https://tele.kenobit.it/)

-------------------------------------
This site is part of a webring: a collection of personal websites and wikis. Browse a [random page](https://lieu.cblgh.org/random) or check the [full list](https://webring.xxiivv.com/).
