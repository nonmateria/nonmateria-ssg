
Tutorials / Articles / Resources

* [saint11.org](https://saint11.org/) is full of tutorials
* slynyrd's pixel blog [tutorials](https://www.slynyrd.com/pixelblog-catalogue)
* arne's [advices](https://androidarts.com/pixtut/pixelart.htm)
* cure's pixel art tutorial [on pixeljoint](https://pixeljoint.com/forum/forum_posts.asp?TID=11299)
* more tutorials from [gas13.ru](http://gas13.ru/v3/tutorials/)
* game developer's guide to graphical projections [pt1](https://medium.com/retronator-magazine/game-developers-guide-to-graphical-projections-with-video-game-examples-part-1-introduction-aa3d051c137d
) / [pt2](https://medium.com/retronator-magazine/game-developers-guide-to-graphical-projections-with-video-game-examples-part-2-multiview-8e9ad7d9e32f)
* [pixelogic's book](https://michafrar.gumroad.com/l/pixel-logic) (written a bit like a series of tutorials but worth the price) 
* big [thread of tutorials](https://pixeljoint.com/forum/forum_posts.asp?TID=5692) on pixeljoint 
* an [interesting thread](https://pixeljoint.com/forum/forum_posts.asp?TID=12795) on Dawnbringer 16 color palette
* how they made [KOF XII pixel art](https://kofaniv.snk-corp.co.jp/english/info/15th_anniv/2d_dot/creation/index.php)
* Mark Ferrari talk for GDC, [on youtube](https://www.youtube.com/watch?v=aMcJ1Jvtef0) 

------------------------------------

Platforms 

* [lospec](https://lospec.com/)
* [pixeljoint](https://pixeljoint.com/)

------------------------------------

Some of my favourite artists

* [6VCR](https://6vcr.com/)
* [apo+](https://apolism.tumblr.com) (mixes pixel art and blender)
* [deceiver](https://pixeljoint.com/p/64720.htm?sec=icons)
* [finlal](https://pixeljoint.com/p/45404.htm?sec=icons)
* [fool](https://pixeljoint.com/p/6741.htm?sec=icons) ([also](https://www.deviantart.com/fool/gallery))
* [iLKke](https://pixeljoint.com/p/9270.htm?sec=icons) (also [here](https://ilkke.net/heartfolio/2d.html))
* [kartonnnyi](https://pixeljoint.com/p/154006.htm?sec=icons)
* [mark ferrari](https://www.markferrari.com/image-archives)
* [mrmo tarius](https://mrmotarius.itch.io/)
* [marine beaufils](https://www.marine.st/en) (embroidery)
* [penusbmic](https://penusbmic.itch.io/)
* [ptoing](https://pixeljoint.com/p/2191.htm?sec=icons)
* [kirokaze](https://kirokazepixel.tumblr.com/)
* rexbeng [PETSCII](https://demozoo.org/sceners/44025/) art
* [saint11.org](https://saint11.org/)
* [slynyrd](https://www.slynyrd.com)
* [syosa](https://pixeljoint.com/p/16406.htm?sec=icons) ([interview](https://pixeljoint.com/2009/10/03/2938/Pixel_Artist_-_Syosa.htm5))
* [valenberg](https://valenberg.tumblr.com)
* [waneella](https://waneella.tumblr.com)

------------------------------------

References : Anatomy / Fundamentals

* [simple forms 3d models](https://practicedrawingthis.com/cgi-bin/3d-models-index.cgi) to copy, useful for rotation exercises
* planes of the head reference 3d models, [masculine](https://www.artstation.com/artwork/GX3Ax1) / [feminine](https://www.artstation.com/artwork/6a0Par) versions
* [inner body](https://www.innerbody.com/htm/body.html) 3d atlases for human [muscles](https://human.biodigital.com/viewer/?id=5HQ0&ui-anatomy-descriptions=true&ui-anatomy-pronunciations=true&ui-anatomy-labels=true&ui-audio=true&ui-chapter-list=false&ui-fullscreen=true&ui-help=true&ui-info=true&ui-label-list=true&ui-layers=true&ui-loader=circle&ui-media-controls=full&ui-menu=true&ui-nav=true&ui-search=true&ui-tools=true&ui-tutorial=false&ui-undo=true&ui-whiteboard=true&initial.none=true&disable-scroll=false&dk=edac91d2443ff104ab74174b3047b00937a21960&paid=o_25ed890c) and [skeleton](https://human.biodigital.com/viewer/?id=5HPv&ui-anatomy-descriptions=true&ui-anatomy-pronunciations=true&ui-anatomy-labels=true&ui-audio=true&ui-chapter-list=false&ui-fullscreen=true&ui-help=true&ui-info=true&ui-label-list=true&ui-layers=true&ui-loader=circle&ui-media-controls=full&ui-menu=true&ui-nav=true&ui-search=true&ui-tools=true&ui-tutorial=false&ui-undo=true&ui-whiteboard=true&initial.none=true&disable-scroll=false&dk=edac91d2443ff104ab74174b3047b00937a21960&paid=o_25ed890c)
* [https://x6ud.github.io](https://x6ud.github.io/#/): animal skulls 3d models

------------------------------------

Reference : Poses

* [posemaniacs](https://www.posemaniacs.com/): 3d viewable poses
* [posespace](https://www.posespace.com/posetool/): pose library
* [adorkastock](https://www.adorkastock.com/pose/): more free pose references
* [cubebrush](https://cubebrush.co/): you can buy references for $$$, has some freebies

------------------------------------

References : Animation

* [bodies in motion](https://www.bodiesinmotion.photo) include motions and heads shot from different angles, and also all the animations from the Muybridge's books (only few items are free)
* [sakuga booru](https://www.sakugabooru.com/) 
* you can also use animations [on mixamo](https://www.mixamo.com/#/?page=1&type=Motion%2CMotionPack) as reference

------------------------------------

References : Royalty Free Photos

* [pixabay](https://pixabay.com/)
* [unsplash](https://unsplash.com/)

------------------------------------

References : Archives 

* [the Spriters Resource](https://www.spriters-resource.com/) : a treasure trove of spritesheet dumped from pixel art videogames
* [artvee](https://artvee.com/) : curated archive of public domain classical and modern art
