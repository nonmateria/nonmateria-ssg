
General

* check the available [Jams](https://itch.io/jams)
* how to make good [small games](https://farawaytimes.blogspot.com/2023/02/how-to-make-good-small-games.html)
* gamedev [map](https://www.gamedevmap.com)

-------------------------------------

From the the GDC [vault](https://www.gdcvault.com/browse/)

* [building a better jump](https://www.gdcvault.com/play/1023559/Math-for-Game-Programmers-Building)
* [stop getting lost](https://www.gdcvault.com/play/1027206/Stop-Getting-Lost-Make-Cognitive): make cognitive maps, not levels 

-------------------------------------

Code-related 

* The guide to implementing [2D platformers](http://higherorderfun.com/blog/2012/05/20/the-guide-to-implementing-2d-platformers/)
* [A* pathfinding for beginners](http://csis.pace.edu/~benjamin/teaching/cs627/webfiles/Astar.pdf) by Patrick Lester
* the absolute minimum you should know [about unicode](https://tonsky.me/blog/unicode/)
* about the uses of [the Bresenham algorithm](https://deepnight.net/tutorial/bresenham-magic-raycasting-line-of-sight-pathfinding/) ( pseudocode on [wikipedia](https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm#All_cases) )
* what you should know [about gamma](https://blog.johnnovak.net/2016/09/21/what-every-coder-should-know-about-gamma/)
* [ditherpunk](https://surma.dev/things/ditherpunk/) : about monochrome image dithering 
* [easing](https://easings.net/) functions
* a good article ( with code ) about [catmull-rom splines](https://qroph.github.io/2018/07/30/smooth-paths-using-catmull-rom-splines.html) 

-------------------------------------

about Scrolling Shooters ( aka STGs or "shmups" )

* [shmups wiki](https://shmups.wiki)
* ^^^ expecially [Boghog's bullet hell shmup 101](https://shmups.wiki/library/Boghog%27s_bullet_hell_shmup_101) 
* [The Electric Underground](https://www.youtube.com/@TheElectricUnderground) has some very in depth critique of various shmups game design

-------------------------------------


Bitmap fonts selection

* [fairfax](https://www.kreativekorp.com/software/fonts/fairfax/)
* [greybeard](https://github.com/flowchartsman/greybeard)
* [pixolde](https://fontenddev.com/fonts/pixolde/)
* [typecast](https://fontenddev.com/fonts/typecast/)


-------------------------------------

Accessibility

* [atkinson hyperlegible](https://brailleinstitute.org/freefont) font, for a file working in love2d take it from [google fonts](https://fonts.google.com/specimen/Atkinson+Hyperlegible)
* [opendyslexic](https://opendyslexic.org) font

-------------------------------------

Engines and frameworks:

* [defold](https://defold.com/)
* [godot engine](https://godotengine.org/)
* [love2d](https://www.love2d.org/)
* [raylib](https://www.raylib.com/)

Smaller Engines
* [bitsy](http://www.make.bitsy.org/)
* [decker](https://beyondloom.com/decker/index.html)
* [downpour](http://downpour.games) 
* [pico8](https://www.lexaloffle.com/pico-8.php)
* [uxn](https://100r.co/site/uxn.html)
