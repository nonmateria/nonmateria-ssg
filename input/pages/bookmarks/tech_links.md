
Some of the most meaningful articles or short essays i read about technology:

* [Programming is Forgetting](http://opentranscripts.org/transcript/programming-forgetting-new-hacker-ethic/) : Toward a New Hacker Ethic 
 
* [The Work of Creation in the Age of AI]( http://perfors.net/blog/creation-ai/ ) 

* [The Cost of a Tool](https://blog.edwardloveall.com/cost-of-a-tool)

* [We're sorry we created the Torment Nexus](http://www.antipope.org/charlie/blog-static/2023/11/dont-create-the-torment-nexus.html)

* [How Technology is Hijacking Your Mind](https://medium.com/thrive-global/how-technology-hijacks-peoples-minds-from-a-magician-and-google-s-design-ethicist-56d62ef5edf3) - from a Magician and Google Design Ethicist ( linked also on [scribe.rip](https://scribe.rip/thrive-global/how-technology-hijacks-peoples-minds-from-a-magician-and-google-s-design-ethicist-56d62ef5edf3) )

* [I went to see a movie](https://signalvnoise.com/svn3/i-went-to-see-a-movie-and-instead-i-saw-the-future/), and instead I saw the future

* [Frugal Computing](https://limited.systems/articles/frugal-computing/)

* [Computing within Limits](https://computingwithinlimits.org)

* [Designing for Disassembly](https://wiki.xxiivv.com/site/permacomputing.html#disassembly)

* [Complex Systems Fail](https://how.complexsystems.fail/)
