#!/bin/bash
echo "clearing destination folder..."
mkdir -p /home/$USER/htdocs/nonmateria-ssg/html_output/data
rm -rf /home/$USER/htdocs/nonmateria-ssg/html_output/data
echo "getting data..."
wget -r -m --no-parent -erobots=off --reject="index.html*" http://nonmateria.com/data/
echo "moving data..."
mv nonmateria.com/data/ /home/$USER/htdocs/nonmateria-ssg/html_output
echo "clearing..."
rm -rf nonmateria.com
echo "done"
exit
