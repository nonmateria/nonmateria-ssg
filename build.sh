#!/usr/bin/fish

set SCRIPTFOLDER "$HOME/htdocs/nonmateria-ssg"
set OUTFOLDER "$SCRIPTFOLDER/html_output"
set MAXPOSTSPERPAGE 3

#--------------------------------------------------------------------------------
set GENERAL_THUMB "avatars/avatar_og.png"

function og_image_path
    set filesource "$argv[1]"
    set og_thumbfile "$GENERAL_THUMB"

    if test -n $filesource
        set post_og_thumb ( cat $filesource | grep "og_thumb=" )
        if test -n $post_og_thumb
            set og_thumbfile (echo $post_og_thumb | cut -d '=' -f2)
        end
    end

    echo "<meta property=\"og:image\" content=\"http://nonmateria.com/data/$og_thumbfile\" />"
end

function open_page #-------------------------------------------------------------
    # arg 1 should be page path to get thumb
    # arg 2 should be the title
    set text '<!DOCTYPE html>'
    set -a text '\n<html>'
    set -a text '\n<head>'
    set -a text '\n<meta charset="utf-8" />'
    if test -n $argv[2]
        set -a text "\n<title>[ $argv[2] ]</title>"
    else
        set -a text "\n<title>[ nonmateria ]</title>"
    end
    set -a text "\n"(cat input/base/head.html)

    set -a text (og_image_path "$argv[1]")

    # TODO get width and height someway from image 
    #set -a text '\n<meta property="og:image:height" content="230" />'
    #set -a text '\n<meta property="og:image:width" content="230" />'

    set -a text "\n</head>"
    set -a text "\n<body>"
    echo $text
end

function close_page
    echo "</body></html>"
end

function navigation_header #-----------------------------------------------------
    # argument 1 is the tab to highlight
    set -l tabs "home" "follow" "devlog" "links" 
    set text "<header>"

    for x in $tabs
        if test "$x" = "$argv[1]"
            set pretab "&nbsp;[&nbsp;"
            set posttab "&nbsp;]&nbsp;"
        else
            set pretab "&nbsp;"
            set posttab "&nbsp;"
        end

        switch $x
            case feed main home
                set -a text "$pretab<a href=\"./index.html\">home</a>$posttab"
            case links
                set -a text "$pretab<a href=\"./links.html\">links</a>$posttab"
            case follow
                set -a text "$pretab<a href=\"./follow.html\">+follow</a>$posttab"

            case devlog
                set -a text "$pretab<a href=\"./devlog.html\">devlog</a>/<a href=\"rss.xml\">RSS</a>$posttab"
                
            case webring
                set -a text "$pretab<a href=\"https://webring.xxiivv.com/\">webring</a>$posttab"
        end
    end

    set -a text "</header>"
    echo $text
end

function build_pages #-----------------------------------------------------------
    printf "building pages"
    mkdir -p "$OUTFOLDER/pages"

    for x in input/pages/**.md # --- builds markdown pages ---
        set name (basename $x .md)
        set text (open_page "$x" "$name")
        set -a text "\n"(navigation_header $name)
        set -a text "<section>"
        set text (echo $text | sed -e "s|SITEROOTPATH|.|g")
        echo -e "$text" >"$OUTFOLDER/$name.html"
        pandoc $x -f commonmark -t html --ascii >>"$OUTFOLDER/$name.html"
        set text "</section>"
        set -a text "\n"(close_page)
        set text (echo $text | sed -e "s|SITEROOTPATH|.|g") 
        echo -e "$text" >> "$OUTFOLDER/$name.html"
        
        sed -i 's|<hr />|</section><section>|g' "$OUTFOLDER/$name.html"
        printf "."
    end

    for x in input/pages/*.html # --- builds html pages ---
        set name (basename $x .html)
        set outname (basename $x)

        set text (open_page "$x" "$name")
        if test "$name" = "index"
            set -a text "\n"(navigation_header "home")
        else
            set -a text "\n"(navigation_header $name)
        end
        set -a text "\n"(cat "$x")
        set -a text "\n"(close_page)
        set text (echo $text | sed -e "s|SITEROOTPATH|.|g")

        echo -e "$text" >"$OUTFOLDER/$outname"
        printf "."
    end

    printf "\n"
end

function build_redirect #--------------------------------------------------------
    set -l from "$argv[1]"
    set -l dest "$argv[2]"
    echo "creating redirect from $from to $dest"
    set text "<!DOCTYPE html>"
    set -a text "\n<html>\n<head>\n<meta charset=\"utf-8\" />"
    set -a text "\n<title>[ nonmateria ]</title>"
    set -a text "\n"(cat input/base/head.html)
    set -a text (og_image_path)
    set -a text "\n<meta property=\"og:image:height\" content=\"230\" />"
    set -a text "\n<meta property=\"og:image:width\" content=\"230\" />"
    set -a text "\n<meta http-equiv=\"refresh\" content=\"0;url=$dest\" />"
    set -a text "\n</head>"
    set -a text "\n<body>"
    set -a text "\n<br/><br/>redirecting to $dest..."
    set -a text "\n</body>"
    set -a text "\n</html>"
    set text (echo $text | sed -e "s|SITEROOTPATH|.|g")
    echo -e $text >"$OUTFOLDER/$from"
end



#--------------------- FUNCTIONS FOR DEVLOG -------------------------------------
function post_date
    set -l post_path $argv[1]
    set -l post_name (basename $post_path)
    set date_str (string sub -s 1 -l 10 $post_name | string replace -a _ / )
    echo "<br><br><div style=\"text-align:right\">posted : <a href=\"$post_name\">$date_str</a> </div><br>"
end

function navigation_footer #-----------------------------------------------------
    set numpage "$argv[1]"
    set maxpage "$argv[2]"
    set channel "$argv[3]"
    set footer "<footer>"

    if test "$numpage" -ne "1"
        set -l prev (math "$numpage-1")
        set -a footer "<a href=\"devlog_$prev.html\">prev</a>"
    end

    for i in (seq $maxpage)
        if test "$i" -eq "$numpage"
            set -a footer "<a href=\"devlog_$i.html\">[$i]</a>"
        else
            set -a footer "<a href=\"devlog_$i.html\">$i</a>"
        end
    end

    if test "$numpage" -ne "$maxpage"
        set -l next (math "$numpage+1")
        set -a footer "<a href=\"devlog_$next.html\">next</a>"
    end

    set -a footer " / <a href=\"./rss.xml\">rss</a>"
    set -a footer "</footer>"

    echo $footer
end

function build_feed #------------------------------------------------------------
    printf "building devlog\n"

    set title "devlog"
    set header_tag "devlog"

    set list "devlog/"(ls -1 "$SCRIPTFOLDER/input/devlog/" | sort -r)

    set posts_len (count $list)
    set max_pages (math "floor($posts_len / $MAXPOSTSPERPAGE)")

    # adds the last page if needed 
    if test (math -s0 "$posts_len % $MAXPOSTSPERPAGE") -ne "0"
        set max_pages (math "$max_pages+1")
    end

    for i in ( seq "$max_pages" )
        printf "building page $i\n"

        set start (math "(($i-1)*$MAXPOSTSPERPAGE)+1" )
        set stop (math "(($i)*$MAXPOSTSPERPAGE)" )
        # often the last page has less posts
        if test "$stop" -gt "$posts_len"
            set stop $posts_len
        end

        set text (open_page "" "$title")
        set -a text "\n"(navigation_header "$header_tag" )

        for p in $list[$start..$stop]
            set -a text "\n<section style=\"max-width:960px;\">"
            set -a text "\n"(cat "input/$p")
            set -a text "\n"(post_date $p)
            set -a text "\n</section>"
        end

        set -a text "\n"(navigation_footer $i $max_pages "devlog")
        set -a text "\n"(close_page)
        set text (echo $text | sed -e "s|SITEROOTPATH|.|g")

        echo -e "$text" >"$OUTFOLDER/devlog_$i.html"
    end
    cp "$OUTFOLDER/devlog_1.html" "$OUTFOLDER/devlog.html"
end

function build_posts #-----------------------------------------------------------
    printf "building posts"
    mkdir -p "$OUTFOLDER/devlog"

    set list "devlog/"(ls -1 "$SCRIPTFOLDER/input/devlog/" | sort -r)

    for p in $list
        set here (basename $p)
        set -l title ( basename "$p" .html | string sub -s 13 | string replace -a _ " " )
        set    text (open_page "$SCRIPTFOLDER/input/$p" "$title")
        set -a text "\n"(navigation_header "post" )
        #set -a text "\n<section>"
        set -a text "\n<section style=\"max-width:960px;\">"
        set -a text "\n"(cat "input/$p" )
        set -a text "\n"(post_date "$p" )
        set -a text "\n</section>"
        set -a text "\n"(close_page)
        set    text (echo $text | sed -e "s|SITEROOTPATH|.|g")
        echo -e "$text" > "$OUTFOLDER/$here"

        printf "."
    end

    
    printf "\n"
end


function build_rss #-------------------------------------------------------------
    printf "building RSS"

    # sorts by the forth field, using / as delimiter 

    set list (ls -1 "$SCRIPTFOLDER/input/devlog/" | sort -r)

    set text "<?xml version='1.0' encoding='UTF-8' ?>"
    set -a text "\n<rss version=\"2.0\">"
    set -a text "\n<channel>"
    set -a text "\n\t<title>nonmateria.com RSS</title>"
    set -a text "\n\t<link>http://nonmateria.com/main/index.html</link>"
    set -a text "\n\t<description>in chronological order</description>"
    set -a text "\n\t<language>en-us</language>"
    set -a text "\n\t<docs>http://backend.userland.com/rss</docs>"

    for p in $list[1..16] # latest 16 posts 
        if test -n $p
            set -l year (string sub -s 1 -l 4 $p)
            set -l month (string sub -s 6 -l 2 $p)
            set -l day (string sub -s 9 -l 2 $p)
            set -l title ( basename "$p" .html | string sub -s 13 | string replace -a _ " " )
            set -l pagelink "$p"
            set -l RFC822_time (date --date=$month/$day/$year -R)

            set -a text "\n\t\t<item>"
            set -a text "\n\t\t\t<title>$title</title>"
            set -a text "\n\t\t\t<description><![CDATA["
            set -a text "\n\t\t\t<section>"
            set -a text "\n\t\t\t"(cat "input/devlog/$p")
            set -a text "\n\t\t\t</section>"
            set -a text "\n\t\t\t]]></description>"
            set -a text "\n\t\t\t<pubDate>$RFC822_time</pubDate>"
            set -a text "\n\t\t\t<link>http://nonmateria.com/$pagelink</link>"
            set -a text "\n\t\t</item>"

            printf "."
        end
    end

    set -a text "\n\t</channel>"
    set -a text "\n</rss>"
    set text (echo $text | sed -e "s|SITEROOTPATH|http://nonmateria.com|g")
    echo -e "$text" >"$OUTFOLDER/rss.xml"
    printf "\n"
end

#--------------------- EXECUTES ALL THE FUNCTIONS -------------------------------
cd $SCRIPTFOLDER

rm $OUTFOLDER/*.html
build_pages

build_feed
build_posts
build_rss
cp "$OUTFOLDER/rss.xml" "$OUTFOLDER/journal/rss.xml"

cp "input/base/style.css" "$OUTFOLDER/style.css"
cp "input/base/robots.txt" "$OUTFOLDER/robots.txt"
cp "input/base/htaccess" "$OUTFOLDER/.htaccess"

# retrocompatibiliy
build_redirect "np-samples.html" "http://nonmateria.com/nm-samples.html"

exit