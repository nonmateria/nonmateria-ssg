#!/usr/bin/fish

set SCRIPTFOLDER "$HOME/htdocs/nonmateria-ssg"
set OUTFOLDER "$SCRIPTFOLDER/html_output"
set MAXPOSTSPERPAGE 6
set CATEGORIES "ink" "gizmos" "music" "visuals"

#--------------------------------------------------------------------------------
function clear_folders
    printf "clearing folders"
	for dir in $CATEGORIES
        rm $OUTFOLDER/$dir/*.html
        printf "."
    end
    printf "."
    rm $OUTFOLDER/posts/*.html
    printf "."
    rm $OUTFOLDER/main/*.html
    printf "\n"
end

#--------------------------------------------------------------------------------
set GENERAL_THUMB "avatars/avatar_og.png"

function og_image_path
    set filesource "$argv[1]"
   	set og_thumbfile "$GENERAL_THUMB"
   	
	if test -n $filesource
		set post_og_thumb ( cat $filesource | grep "og_thumb=" )
		if test -n $post_og_thumb
			set og_thumbfile (echo $post_og_thumb | cut -d '=' -f2)
		end
	end

	echo "<meta property=\"og:image\" content=\"http://nonmateria.com/data/$og_thumbfile\" />"
end

function date_and_category 
	set -l post_path $argv[1]
	if test -z  $argv[2]; or test $argv[2] -eq "1"
		set pagelink "index"
	else
		set pagelink "page$argv[2]"
	end

	set -l post_split (string split / $argv[1])
	# channel in [1], name in [2]
	set -l post_dir $post_split[1]
	set -l post_name $post_split[2]

	if test -z $argv[3] 
		#for setting "permalink" to hide date
		set date_str (string sub -s 1 -l 10 $post_name | string replace -a _ / )
	else 
		set date_str $argv[3]
	end
	
	echo "<br><br><div style=\"text-align:right\"><a href=\"../posts/$post_name\">$date_str</a> / posted in <a href=\"../$post_dir/$pagelink.html\">$post_dir</a></div>"
end

function find_dir_page 
	# arg 1 is dir/postname.html 
	set -l post_split (string split / $argv[1])
	set post_dir $post_split[1]
	set post_name $post_split[2]
	set postnum "1" 
	set pagenum "1"
   
	for p in (ls -1 "$SCRIPTFOLDER/input/journal/$post_dir" | sort -r)
		if test "$p" = "$post_name"    
			set pagenum (math -s0 "(($postnum-1)/$MAXPOSTSPERPAGE)+1" )
		end
		set postnum (math "$postnum+1")
	end

	echo $pagenum
end

function open_page #-------------------------------------------------------------
	# arg 1 should be page path to get thumb
	# arg 2 should be the title
	set    text '<!DOCTYPE html>'
	set -a text '\n<html>'
	set -a text '\n<head>'
	set -a text '\n<meta charset="utf-8" />'
	if test -n $argv[2] 
		set -a text "\n<title>[ $argv[2] ]</title>"
	else
		set -a text "\n<title>[ nonmateria ]</title>"
	end 
	set -a text "\n"(cat input/base/head.html) 

	set -a text (og_image_path "$argv[1]") 
	
    # TODO get width and height someway from image 
	#set -a text '\n<meta property="og:image:height" content="230" />'
	#set -a text '\n<meta property="og:image:width" content="230" />'

	set -a text "\n</head>"
	set -a text "\n<body>"
	echo $text
end

function close_page
	echo "</body></html>"
end

function navigation_header #-----------------------------------------------------
	# argument 1 is the tab to highlight
	set -l tabs "feed" "infos" "vessel" "webring"
	set text "<header>"

	for x in $tabs
		if test "$x" = "$argv[1]"
			set pretab "[ "
			set posttab " ]&nbsp;"
		else
			set pretab ""
			set posttab "&nbsp;"
		end
		
		switch $x 
		case feed main
			set -a text "$pretab<a href=\"../main/index.html\">main</a>$posttab"
		case infos
			set -a text "$pretab<a href=\"../pages/infos.html\">infos</a>$posttab"	
		case vessel 
			set -a text "$pretab<a href=\"../pages/vessel.html\">vessel</a>$posttab"
		case webring
			set -a text "$pretab<a href=\"https://webring.xxiivv.com/\">webring</a>$posttab"
		end
	end

	set -a text "</header>"
	echo $text
end 

function navigation_footer #-----------------------------------------------------
	set numpage "$argv[1]"
	set maxpage "$argv[2]"
	set channel "$argv[3]"
	set footer "<footer>"

	if test "$numpage" -ne "1"
		set -l prev (math "$numpage-1")
		set -a footer "<a href=\"page$prev.html\">prev</a>"
	end

	for i in (seq $maxpage)
		if test "$i" -eq "$numpage"
			set -a footer "<a href=\"page$i.html\">[$i]</a>"
		else
			set -a footer "<a href=\"page$i.html\">$i</a>"
		end
	end
	
	if test "$numpage" -ne "$maxpage"
		set -l next (math "$numpage+1")
		set -a footer "<a href=\"page$next.html\">next</a>"
	end

	if test "$channel" = "main"
		set -a footer " / <a href=\"../rss.xml\">rss</a>"	
	else
		set -a footer " / <a href=\"archive.html\">all</a>"  
	end

	set -a footer "</footer>"

	echo $footer
end

function build_pages #-----------------------------------------------------------
	printf "building pages"
	mkdir -p "$OUTFOLDER/pages"

	for x in input/pages/**.md # --- builds markdown pages ---
		set name (basename $x .md)
		set    text (open_page "$x" "$name")
		set -a text "\n"(navigation_header $name)
		set -a text "<section>"
		set text (echo $text | sed -e "s|SITEROOTPATH|..|g") 		
		echo -e "$text" > "$OUTFOLDER/pages/$name.html"
		pandoc $x -f commonmark -t html --ascii >> "$OUTFOLDER/pages/$name.html"
		set text "</section>"
		set -a text "\n"(close_page)
		set text (echo $text | sed -e "s|SITEROOTPATH|..|g") 		
		echo -e "$text" >> "$OUTFOLDER/pages/$name.html"
		printf "."
	end
	
	for x in input/pages/*.html  # --- builds html pages ---
		set name (basename $x .html)
		set outname (basename $x)

		set    text (open_page "$x" "$name")
		set -a text "\n"(navigation_header $name)
		set -a text "\n"(cat "$x")
		set -a text "\n"(close_page)
		set text (echo $text | sed -e "s|SITEROOTPATH|..|g") 		

		echo -e "$text" > "$OUTFOLDER/pages/$outname"
		printf "."
	end
	
	cp "$OUTFOLDER/pages/infos.html" "$OUTFOLDER/pages/index.html" 

	printf "\n"
end

function build_feed #------------------------------------------------------------
	set channel_name "$argv[1]"
	printf "building $channel_name"
	set header_tag "$channel_name"
	set title "$channel_name"

	mkdir -p "$OUTFOLDER/$channel_name"

	if test "$channel_name" = "main"
		set list (cat input/journal/feed.list)
		set header_tag "feed"
		set title "nonmateria"
	else
		set list "$argv[1]/"(ls -1 "$SCRIPTFOLDER/input/journal/$argv[1]" | sort -r)    
	end

	set posts_len (count $list)
	set max_pages (math "floor($posts_len / $MAXPOSTSPERPAGE)")

	# adds the last page if needed 
	if test (math -s0 "$posts_len % $MAXPOSTSPERPAGE") -ne "0"
		set max_pages (math "$max_pages+1")
	end
    
	for i in ( seq "$max_pages" )
		set start (math "(($i-1)*$MAXPOSTSPERPAGE)+1" ) 
		set stop (math "(($i)*$MAXPOSTSPERPAGE)" ) 
		# often the last page has less posts
		if test "$stop" -gt "$posts_len" 
			set stop $posts_len
		end

		set    text (open_page "" "$title")
		set -a text "\n"(navigation_header "$header_tag" )

		for p in $list[$start..$stop]
			#set -a text "\n<section>"
			set -a text "\n"(cat "input/journal/$p")
			if test "$channel_name" = "main"
				# this will search for the correct category page
				set -l dirpage (find_dir_page "$p")
				set -a text "\n"(date_and_category $p $dirpage)
			else
				set -a text "\n"(date_and_category $p $i)
			end
			set -a text "\n</section>"
		end

		set -a text "\n"(navigation_footer $i $max_pages "$channel_name") 
		set -a text "\n"(close_page)
		set    text (echo $text | sed -e "s|SITEROOTPATH|..|g") 		

		echo -e "$text" > "$OUTFOLDER/$channel_name/page$i.html"
		printf "."
	end
	cp "$OUTFOLDER/$channel_name/page1.html" "$OUTFOLDER/$channel_name/index.html"
	printf "\n"
end

function build_posts #-----------------------------------------------------------
    printf "building posts"
    mkdir -p "$OUTFOLDER/posts"

	set postcat $CATEGORIES
	set -a postcat "unlisted"
	for dir in $postcat
		set postnum "1"
		set list (ls -1 "$SCRIPTFOLDER/input/journal/$dir" | sort -r)    

		for p in $list
			if test -n $p
				set -l pagenum (math -s0 "(($postnum-1)/$MAXPOSTSPERPAGE)+1" )
				set -l title ( basename "$p" .html | string sub -s 13 | string replace -a _ " " )
				set    text (open_page "$SCRIPTFOLDER/input/journal/$dir/$p" "$title")
				set -a text "\n"(navigation_header "post" )
				#set -a text "\n<section>"
				set -a text "\n"(cat "input/journal/$dir/$p" )
				set -a text "\n"(date_and_category "$dir/$p" "$pagenum" )
				set -a text "\n</section>"
				set -a text "\n"(close_page)
				set    text (echo $text | sed -e "s|SITEROOTPATH|..|g") 		
				echo -e "$text" > "$OUTFOLDER/posts/$p"

				set postnum (math "$postnum + 1")
				printf "."
			end
		end
	end
    
    printf "\n"
end

function build_archives #--------------------------------------------------------
	printf "building archives"
	set last_year "1995"

	for dir in $CATEGORIES
		set list (ls -1 "$SCRIPTFOLDER/input/journal/$dir")    

		set    text (open_page "" "$dir archives")
		set -a text "\n"(navigation_header "$dir archives" )
		set -a text "\n<section>"

		for p in $list
			set -l post_date (string sub -s 1 -l 10 $p | string replace -a _ / )
			set -l post_year (string sub -s 1 -l 4 $p )
			if test "$last_year" != "$post_year"
				set last_year "$post_year"
				set -a text "\n<br/>"
			end

			set -l post_name ( basename "$p" .html | string sub -s 12 | string replace -a _ " " )
			set -a text "\n$post_date <a href=\"../posts/$p\">$post_name</a><br/>"
		end

		set -a text "\n<br/>"
		set -a text "\n</section>"
		set -a text "\n"(close_page)
		set    text (echo $text | sed -e "s|SITEROOTPATH|..|g") 
		echo -e "$text" > "$OUTFOLDER/$dir/archive.html"
		printf "."
	end

	printf "\n"
end

function build_rss #-------------------------------------------------------------
	printf "building RSS"

    # sorts by the forth field, using / as delimiter 
	set list (ls input/journal/$CATEGORIES[1]/*.html input/journal/$CATEGORIES[2]/*.html input/journal/$CATEGORIES[3]/*.html input/journal/$CATEGORIES[4]/*.html input/journal/rss_only/*.html | sort -t/ -k4 -r)  

	set text "<?xml version='1.0' encoding='UTF-8' ?>"
	set -a text "\n<rss version=\"2.0\">"
	set -a text "\n<channel>"
	set -a text "\n\t<title>nonmateria.com RSS</title>"
	set -a text "\n\t<link>http://nonmateria.com/main/index.html</link>"
	set -a text "\n\t<description>in chronological order</description>"
	set -a text "\n\t<language>en-us</language>"
	set -a text "\n\t<docs>http://backend.userland.com/rss</docs>"

	for p in $list[1..16] # latest 16 posts 
		if test -n $p
			set -l post_split (string split / $p)
			set post_dir $post_split[3]
			set post_name $post_split[4]
			set -l year (string sub -s 1 -l 4 $post_name)
			set -l month (string sub -s 6 -l 2 $post_name)
			set -l day (string sub -s 9 -l 2 $post_name)
			set -l title ( basename "$post_name" .html | string sub -s 13 | string replace -a _ " " )
			set -l pagelink "posts/$post_name"
			set -l RFC822_time (date --date=$month/$day/$year -R)

			set -a text "\n\t\t<item>"
			set -a text "\n\t\t\t<title>$title</title>" 
			set -a text "\n\t\t\t<description><![CDATA["
			set -a text "\n\t\t\t"(cat "$p")
			set -a text "\n\t\t\t</section>"
			set -a text "\n\t\t\t]]></description>"
			set -a text "\n\t\t\t<pubDate>$RFC822_time</pubDate>"
			set -a text "\n\t\t\t<link>http://nonmateria.com/$pagelink</link>"
			set -a text "\n\t\t</item>"

			printf "."
		end
	end

	set -a text "\n\t</channel>"
	set -a text "\n</rss>"    
	set text (echo $text | sed -e "s|SITEROOTPATH|http://nonmateria.com|g") 
	echo -e "$text" > "$OUTFOLDER/rss.xml"
	printf "\n"
end

function build_redirect #--------------------------------------------------------
    set -l from "$argv[1]"
    set -l dest "$argv[2]"
	echo "creating redirect from $from to $dest"
	set    text "<!DOCTYPE html>"
	set -a text "\n<html>\n<head>\n<meta charset=\"utf-8\" />"
	set -a text "\n<title>[ nonmateria ]</title>"
	set -a text "\n"(cat input/base/head.html) 
	set -a text (og_image_path) 
	set -a text "\n<meta property=\"og:image:height\" content=\"230\" />"
	set -a text "\n<meta property=\"og:image:width\" content=\"230\" />"
	set -a text "\n<meta http-equiv=\"refresh\" content=\"0;url=$dest\" />"
	set -a text "\n</head>"
	set -a text "\n<body>"
	set -a text "\n<br/><br/>redirecting to $dest..."
	set -a text "\n</body>"
	set -a text "\n</html>"
	set text (echo $text | sed -e "s|SITEROOTPATH|.|g") 
	echo -e $text > "$OUTFOLDER/$from"
end

#--------------------- EXECUTES ALL THE FUNCTIONS -------------------------------
cd $SCRIPTFOLDER

clear_folders
build_pages
build_feed "main"
for dir in $CATEGORIES; build_feed $dir; end
build_posts
build_archives
build_rss
build_redirect "index.html" "main/index.html"
cp "input/base/style.css" "$OUTFOLDER/style.css"
cp "input/base/robots.txt" "$OUTFOLDER/robots.txt"
cp "input/base/htaccess" "$OUTFOLDER/.htaccess"

# retrocompatibiliy
build_redirect "about.html" "pages/vessel.html"
build_redirect "pages/contact.html" "vessel.html"
build_redirect "pages/about.html" "infos.html"
build_redirect "pages/more.html" "infos.html"
build_redirect "pages/readme.html" "infos.html"
build_redirect "hexaglyphics.html" "http://nonmateria.com/pages/hexaglyphics.html"
build_redirect "np-samples.html" "http://nonmateria.com/pages/nm-samples.html"
mkdir -p "$OUTFOLDER/journal"
cp "$OUTFOLDER/rss.xml" "$OUTFOLDER/journal/rss.xml"

exit 