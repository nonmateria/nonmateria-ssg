nonmateria-ssg
==============

Here are all the files and the script that generate the [nonmateria.com](http://nonmateria.com) website, and some helpers to fetch and upload data.

The static site generator is a script of about 400 lines of code, made for the [fish](https://fishshell.com/) shell. The CSS for the entire site is less than 1kb. 

This is also a proof that shell scripting, although not very efficient, can be a viable way for generating small to medium websites, expecially the ones not updated that often. The script puts together various bits of html, but you can easily use any kind of markup languages you want, maybe also converting media on the fly while you traverse the folders. 

So next time you are tired of bloated frameworks you don't need, why not to try your shell? 

nonmateria MIT License 2020.
