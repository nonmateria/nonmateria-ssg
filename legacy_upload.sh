#!/usr/bin/fish

set fish_history ""

echo 'the ftp transfer used in this script is not encrypted, are you behind a trusted connection?'
	
while true
    set yn (read -P "[y/n]: ") 

	switch "$yn"
    case Y y yes
    	break
    case N n no
    	 echo "goodbye!" 
    	 exit
    case '*'
    	echo "Please answer yes or no."
    end
end

set ftpuser (read -P "user: " )
set ftppass (read -s -P "password: " )

echo ""

set FOLDERS "main" "ink" "gizmos" "music" "visuals" "posts" "pages"

for f in $FOLDERS
	echo "uploading folder $f"
	ncftpput -R -v -u "$ftpuser" -p "$ftppass" ftp.npisanti.com public_html "~/htdocs/nonmateria-ssg/html_output/$f/"
end

echo "uploading root files"
ncftpput -u "$ftpuser" -p "$ftppass" ftp.npisanti.com public_html ~/htdocs/nonmateria-ssg/html_output/index.html
ncftpput -u "$ftpuser" -p "$ftppass" ftp.npisanti.com public_html ~/htdocs/nonmateria-ssg/html_output/style.css
ncftpput -u "$ftpuser" -p "$ftppass" ftp.npisanti.com public_html ~/htdocs/nonmateria-ssg/html_output/rss.xml
ncftpput -u "$ftpuser" -p "$ftppass" ftp.npisanti.com public_html ~/htdocs/nonmateria-ssg/html_output/robots.txt
ncftpput -u "$ftpuser" -p "$ftppass" ftp.npisanti.com public_html ~/htdocs/nonmateria-ssg/html_output/.htaccess

echo "uploading for retrocompatibility..."
ncftpput -u "$ftpuser" -p "$ftppass" ftp.npisanti.com public_html ~/htdocs/nonmateria-ssg/html_output/hexaglyphics.html
ncftpput -u "$ftpuser" -p "$ftppass" ftp.npisanti.com public_html ~/htdocs/nonmateria-ssg/html_output/about.html
ncftpput -u "$ftpuser" -p "$ftppass" ftp.npisanti.com public_html/journal ~/htdocs/nonmateria-ssg/html_output/journal/rss.xml


exit
